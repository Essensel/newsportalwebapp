package seschool.newsportal.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import seschool.newsportal.repositories.NewsRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class NewsList {
    @Autowired
    private NewsRepository repository;
    private static ArrayList<NewItem> allNews = new ArrayList<>();


    /**
     * вернуть список новостей
     *
     * @return список новостей
     */

    public List<NewItem> getList(int page, int limit) {
  Page<NewItem> resultPage = repository.findAll(PageRequest.of(page-1,limit));
        return resultPage.getContent();
    }

    public List<NewItem> getListOnMainPage(int limit){

        return repository.findAllByShowOnMain(true, PageRequest.of(0,limit)).getContent();
    }



    public int getCount() {
        return (int)this.repository.count();
    }


    public NewItem getById(int id) {
        Optional<NewItem> result = repository.findById((long)id);

        if (repository.findById((long)id).isPresent()) {
            return result.get();
        }
        return null;
    }

    //вернуть список всех новостей, содержащих слово query в заголовке или в тексте
    public List<NewItem> search(String query) {
        //копим новости
     /*   ArrayList<New> result = new ArrayList<>();

        for (New currentNew : this.repository.findAll()) {
            //.toLowerCase() - это мы переводим к нижнему регистру и текст и слово query
            if ((currentNew.getTitle() != null && currentNew.getTitle().toLowerCase().contains(query.toLowerCase()) )||
                    (currentNew.getText()!=null && currentNew.getText().toLowerCase().contains(query.toLowerCase()) )||
                    (currentNew.getWriter()!=null && currentNew.getWriter().toLowerCase().contains(query.toLowerCase()))
            ) {
                result.add(currentNew);
            }
        } */
        return repository.search(query);
    }

    public Long saveNew(NewItem newItem){
        return this.repository.save(newItem).getId();
    }

    public void deleteNew(Long id) {
        this.repository.deleteById(id);
    }
}
