package seschool.newsportal.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import seschool.newsportal.beans.NewItem;

import java.util.List;

public interface NewsRepository extends PagingAndSortingRepository<NewItem, Long> {
    /**
     * Поиск новости по флагу
     * @param show
     * @return
     */
 List<NewItem> findAllByShowOnMain (boolean show);

    /**
     * Поиск новостей по флагу с постраничкой
     * @param show
     * @param pageable
     * @return
     */
 Page<NewItem> findAllByShowOnMain(boolean show, Pageable pageable);
@Query("FROM news WHERE title LIKE %:query% Or text LIKE %:query% ")
 List<NewItem> search(@Param("query") String query);
}
