package seschool.newsportal.repositories;

import org.springframework.data.repository.CrudRepository;
import seschool.newsportal.security.Role;

public interface RolesRepositiry extends CrudRepository<Role, Long> {
}
