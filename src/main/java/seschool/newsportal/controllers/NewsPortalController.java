package seschool.newsportal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import seschool.newsportal.beans.Author;
import seschool.newsportal.beans.NewItem;
import seschool.newsportal.beans.NewsList;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import seschool.newsportal.security.User;
import seschool.newsportal.security.UserService;

import java.util.Optional;
@Controller
public class NewsPortalController {
    @Autowired
    private NewsList newsList;
    @Autowired
    private UserService userService;


    @RequestMapping(value = "/")
    public String showMainPage(ModelMap model) {
        model.addAttribute("newsList", this.newsList.getListOnMainPage(12));
        return "main";
    }


    @RequestMapping(value = "/list")
    public String newsList(ModelMap model, @RequestParam(name = "page", defaultValue = "1") Integer pageNum) {
        //новостей на странице
        int pageSize = 5;

        NewsList newsList = this.newsList;

        model.addAttribute("newsList", newsList.getList(pageNum, pageSize));

        Integer prevPage, nextPage;
        //посчитать страницы для кнопок "вперед" и "назад"

        if (pageNum < 2) {
            prevPage = null;
        } else {
            prevPage = pageNum - 1;
        }

        //всего страниц
        int pageCount = newsList.getCount() / pageSize + 1;
        //если последняя, то null
        if (pageNum >= pageCount) {
            nextPage = null;
        } else {
            nextPage = pageNum + 1;
        }
        model.addAttribute("prevPage", prevPage);
        model.addAttribute("nextPage", nextPage);

        return "list";
    }

    @RequestMapping(value = "/list/{id}")
    public String detailNews(ModelMap model, @PathVariable Integer id) {

        NewsList list = this.newsList;
        model.addAttribute("detailNew", list.getById(id));

        return "detail";
    }


    @RequestMapping(value = "/game/{name}/")
    public String showGame(ModelMap model, @PathVariable String name, @RequestParam(required = false, defaultValue = "all") String platform) {
        model.addAttribute("gameName", name);
        model.addAttribute("platform", platform);
        return "game";
    }

    @RequestMapping(value = {"/newsform/", "/newsform/{id}/"})
    public String showNewsForm(ModelMap model, @PathVariable Optional<Integer> id) {
        if (id.isPresent()) {
            NewItem newToUpdate = this.newsList.getById(id.get());
            if (newToUpdate != null) {
                model.addAttribute("newToUpdate", newToUpdate);
            } else {
                return "redirect:/newsform/";
            }
        } else {
            model.addAttribute("newsworm", null);
        }
        return "newsform";
    }


    @RequestMapping(value = "/search")
    public String searchResult(ModelMap model, @RequestParam(name = "q", defaultValue = "") String query) {

        //добавит результаты поиска в модель
        model.addAttribute("query", query);
        model.addAttribute("searchResult", this.newsList.search(query));

        return "search";
    }


    @RequestMapping(value = "/saveNew/", method = RequestMethod.POST)
    public String addNew(@RequestParam(defaultValue = "") String title,
                         @RequestParam(defaultValue = "") String text,
                         @RequestParam(defaultValue = "") String writer,
                         @RequestParam(defaultValue = "") String date,
                         @RequestParam(defaultValue = "") String picture,
                         @RequestParam(defaultValue = "0") Integer showOnMain,
                         @RequestParam(defaultValue = "0") Long id
    ) {

        NewItem newItem = new NewItem();
        newItem.setTitle(title);
        newItem.setText(text);
        newItem.setWriter(writer);
        newItem.setDate(date);
        newItem.setPicture(picture);
        if (showOnMain == 1) {
            newItem.setShowOnMain(true);
        } else {
            newItem.setShowOnMain(false);
        }
        if (id > 0) {
            newItem.setId(id);
        }
        this.newsList.saveNew(newItem);
        return "redirect:/list/" + newItem.getId();
    }

    @RequestMapping(value = "/deleteNew/{id}/")
    public String deleteNew(ModelMap model, @PathVariable Long id) {


        try {
            newsList.deleteNew(id);
            model.addAttribute("success", true);
        } catch (Exception error) {
            model.addAttribute("success", false);
            model.addAttribute("errorMessage", error.getMessage());
        }

        return "deleteResult";
    }


    @RequestMapping(value = "/adduser/", method = RequestMethod.POST)
    public String registerUser(User newUser) {
        if (newUser.getPassword().equals(newUser.getPasswordConf())) {
            this.userService.saveUser(newUser);
        } else {
            return "redirect:/register/";
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/register/")
    public String showRegisterForm() {
        return "register";
    }


    @RequestMapping(value = "/login/")
    public String showLoginForm() {
        return "login";
    }

}
